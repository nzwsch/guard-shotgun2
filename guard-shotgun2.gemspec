require File.expand_path 'lib/guard/shotgun2/version', __dir__

Gem::Specification.new do |spec|
  spec.name          = "guard-shotgun2"
  spec.version       = Guard::ShotgunVersion::VERSION
  spec.authors       = ["Seiichi Yonezawa"]
  spec.email         = ["hi@nzwsch.com"]
  spec.summary       = 'Guard plugin for Shotgun'
  spec.description   = 'Ridiculously simple guard plugin.'

  spec.files         = `git ls-files -z`.split("\x0")
  spec.require_paths = ["lib"]

  spec.add_dependency "guard",   "~> 2"
  spec.add_dependency "shotgun", "~> 0.9"
end
