require 'guard'
require 'guard/plugin'

module Guard
  class Shotgun2 < Plugin
    def initialize(options = {})
      super
      @options = {
        host: '0.0.0.0',
        port: 9393,
        env:  'development'
      }.merge options
    end

    def start
      UI.info 'Shotshell inserted...'
      @pid = spawn "shotgun",
                   "--host", @options[:host],
                   "--port", @options[:port].to_s,
                   "--env",  @options[:env]
    end

    def stop
      Process.kill 'TERM', @pid
      Process.wait @pid
      @pid = nil
      true
    end
  end
end
