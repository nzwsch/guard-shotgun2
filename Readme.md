# guard-shotgun2

Ridiculously simple guard plugin.

## Options

Shotgun guard has 3 options that you can set like this:

```ruby
guard :shotgun2, port: '5000'
```

Available options:

```ruby
host: '127.0.0.1'  # default '0.0.0.0'
port: '5000'       # default '9393'
env:  'production' # default 'development'
```
